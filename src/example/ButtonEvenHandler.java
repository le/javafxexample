package example;

import javafx.event.Event;
import javafx.event.EventHandler;

public class ButtonEvenHandler implements EventHandler {
    @Override
    public void handle(Event event) {
        System.out.println("Button Even Handler");
    }
}
