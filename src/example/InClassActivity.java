package example;

import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.FlowPane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

public class InClassActivity extends Application {
    @Override
    public void init() throws Exception { super.init(); }

    @Override
    public void stop() throws Exception { super.stop(); }

    @Override
    public void start(Stage stage) throws Exception {
        //Set the stage a title to In Class Activity
        stage.setTitle("In Class Activity");

        // Create a Root node with buttons
        FlowPane rootNode = new FlowPane();

        //Create a Button with label = Hello World
        Button b = new Button("Hello World!");
        EventHandler<MouseEvent> event = new ButtonEvenHandler();
        //Add the action to the button.
        b.addEventHandler(MouseEvent.MOUSE_CLICKED,event);

        //another way to handle the event
        b.setOnAction(new ButtonEvenHandler());

        //b.setOnAction();


        //use lamda expression
        b.setOnAction(e ->{
            System.out.println("Hello World");
        });


        //add a button to root node.
        rootNode.getChildren().add(b);
        //Create a scene with a root node
        Scene scene = new Scene(rootNode, 300, 100);
        //set color for the scene
        scene.setFill(Color.SILVER);

        //Set the scene on the stage.
        stage.setScene(scene);
        //Show the stage and its scene
        stage.show();
    }
    public static void main(String [] args) {
        Application.launch(args);
    }

}
