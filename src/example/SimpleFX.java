package example;

import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

import java.util.ArrayList;

public class SimpleFX extends Application {
    @Override
    public void init() throws Exception { super.init(); }

    @Override
    public void stop() throws Exception { super.stop(); }

    @Override
    public void start(Stage stage) throws Exception {
        //Set the stage a title.
        stage.setTitle("Simle FX Application");

        // Create a Root node with buttons
        FlowPane rootNode = new FlowPane();

        Button b1 = new Button("Flow Pane");
        //b1.setOnAction();
        b1.setOnAction(e->{
            Stage flowPaneStage = new Stage();
            flowPaneStage.setTitle("Flow Pane Demo");
            Scene flowPaneScene = new Scene(flowLayout(),300,200);
            flowPaneStage.setScene(flowPaneScene);
            flowPaneStage.show();
        });
        Button b2 = new Button("HBox Pane");
        b2.setOnAction(e->{
            Stage hBoxPaneStage = new Stage();
            hBoxPaneStage.setTitle("HBox Pane Demo");
            Scene hBoxScene = new Scene(hBoxLayout(),300,200);
            hBoxPaneStage.setScene(hBoxScene);
            hBoxPaneStage.show();
        });
        Button b3 = new Button("VBox Pane");
        b3.setOnAction(e->{
            Stage vBoxPaneStage = new Stage();
            vBoxPaneStage.setTitle("VBox Pane Demo");
            Scene vBoxScene = new Scene(vBoxLayout(),300,200);
            vBoxPaneStage.setScene(vBoxScene);
            vBoxPaneStage.show();
        });
        Button b4 = new Button("Border Pane");
        b4.setOnAction(e->{
            Stage stage1 = new Stage();
            stage1.setTitle("Border Pane Demo");
            Scene scene = new Scene(borderPaneLayout(), 300, 200);
            stage1.setScene(scene);
            stage1.show();
        });

        Button b5 = new Button("Tile Pane");
        b5.setOnAction(e->{
            this.ButtonMouseClickAction(titlePaneLayout(),"Tile Pane Demo");
        });

        rootNode.getChildren().add(b1);
        rootNode.getChildren().add(b2);
        rootNode.getChildren().add(b3);
        rootNode.getChildren().add(b4);
        rootNode.getChildren().add(b5);

        //Create a scene with a root node
        Scene scene = new Scene(rootNode, 300, 100);
        //set color for the scene
        scene.setFill(Color.SILVER);

        //Set the scene on the stage.
        stage.setScene(scene);
        //Show the stage and its scene
        stage.show();
    }
    public void ButtonMouseClickAction(Parent node, String title){
        Stage stage = new Stage();
        stage.setTitle(title);
        Scene scene = new Scene(node,600,500);
        stage.setScene(scene);
        stage.show();
    }
    public FlowPane flowLayout(){
        ArrayList<Button> buttonList = new ArrayList<>();
        FlowPane rootNode = new FlowPane();
        int numberOfButton = 100;
        for(int i = 0; i < numberOfButton; i++ ){
            buttonList.add(new Button("Button " + (i + 1)));
            //add buttons to rootNode
            rootNode.getChildren().add(buttonList.get(i));
        }
        return rootNode;
    }
    public HBox hBoxLayout(){
        int numberOfButton = 100;
        ArrayList<Button> buttonList = buttonFactory(numberOfButton);

        HBox rootNode = new HBox();
        for(int i = 0; i < numberOfButton; i++ ){
            //add buttons to rootNode
            rootNode.getChildren().add(buttonList.get(i));
        }
        return rootNode;
    }

    public VBox vBoxLayout(){
        int numberOfButton = 100;
        ArrayList<Button> buttonList = buttonFactory(numberOfButton);
        VBox rootNode = new VBox();
        for(int i = 0; i < numberOfButton; i++ ){
            //add buttons to rootNode
            rootNode.getChildren().add(buttonList.get(i));
        }
        return rootNode;
    }
    public BorderPane borderPaneLayout(){
        BorderPane borderPane = new BorderPane();
        borderPane.setLeft(new Button("Left"));
        borderPane.setRight(new Button("Right"));
        borderPane.setBottom(new Button("Bottom"));
        borderPane.setTop(new Button("Top"));
        borderPane.setCenter(new Button("Center"));
        return borderPane;
    }
    public TilePane titlePaneLayout(){
        TilePane tilePane = new TilePane();
        tilePane.setPrefColumns(10);
        tilePane.setAlignment(Pos.CENTER);
        for(Button b : buttonFactory(100))
        tilePane.getChildren().add(b);
        return tilePane;
    }

    /**
     * Create an ArrayList of Buttons
     * @param numberOfButton - The number of Button
     * @return - The arrayList of buttons
     */
    public ArrayList<Button> buttonFactory(int numberOfButton){
        ArrayList<Button> buttonList = new ArrayList<>();
        for(int i = 0; i < numberOfButton; i++ ){
            buttonList.add(new Button("Button " + (i + 1)));
        }
        return buttonList;
    }
    public static void main(String[] args) {
        launch(args);
    }

}
