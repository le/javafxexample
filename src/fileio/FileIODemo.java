package fileio;

import java.io.*;

public class FileIODemo {

    public static void main(String [] args) {
        readTextFileOne();
    }

    public static void readTextFileOne() {
        int i = 0;
        String fileName = "textFile.txt";
        try(FileInputStream file = new FileInputStream("textFile.txt")){
            while(i != -1) {
                i = file.read();
                System.out.println((char)i);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public static void readTextFile() {
        String filename = "textFile.txt";
        BufferedReader bufferedReader = null;
        try{
            bufferedReader = new BufferedReader(new FileReader(filename));
            String line = bufferedReader.readLine();
        }
        catch(Exception e) {
            //catch exception
        }
        finally {
            try {
               if(bufferedReader != null)
                   bufferedReader.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
