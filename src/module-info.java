module JavaFXExample {
    requires javafx.fxml;
    requires javafx.controls;
    requires javafx.graphics;
    exports example;
    exports observablelist;
    exports fileio;
    requires junit;
}