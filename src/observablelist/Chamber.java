package observablelist;

import java.util.ArrayList;

public class Chamber {
    private ArrayList<String> doors;
    private String chamberName;

    public Chamber(String chamberName) {
        this.chamberName = chamberName;
        this.doors = new ArrayList<>();
    }

    public ArrayList<String> getDoors() {
        return doors;
    }

    public void addDoor(String door) {
        doors.add(door);
    }
    public void setDoors(ArrayList<String> doors) {
        this.doors = doors;
    }

    public String getChamberName() {
        return chamberName;
    }

    public void setChamberName(String chamberName) {
        this.chamberName = chamberName;
    }
    @Override
    public String toString(){
        return chamberName;
    }
}
