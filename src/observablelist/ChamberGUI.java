package observablelist;

import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.SelectionMode;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;

import java.util.ArrayList;

public class ChamberGUI extends Application {
    //Model
    ObservableList<Chamber> chamberList;
    ObservableList<String> doorList;
    private ListView<Chamber> chamberListView;
    private ListView<String> doorListView;
    private Button addButton;
    public ChamberGUI(){
        initChamber();
        chamberListView = new ListView<Chamber>(chamberList);
        doorList = FXCollections.observableArrayList();
        doorListView = new ListView<String>(doorList);
        addButton = new Button("Add A Door");
    }
    public void initChamber() {

        ArrayList<Chamber> chambers = new ArrayList<>();
        for(int i = 0;i < 5; i++) {
            chambers.add(new Chamber("Chamber" + i));
            chambers.get(i).addDoor("Door " + i);
        }
        chamberList = FXCollections.observableArrayList(chambers);
    }
    @Override
    public void start(Stage stage) throws Exception {
        HBox rootNode = new HBox(20);
        rootNode.getChildren().addAll(chamberListView,doorListView,addButton);
        Scene scene = new Scene(rootNode, 500, 600);

        chamberListView.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);

        chamberListView.addEventHandler(MouseEvent.MOUSE_CLICKED, mouseEvent -> {
            doorList.clear();
            doorList.addAll(chamberListView.getSelectionModel().getSelectedItem().getDoors());

        });

        addButton.setOnAction(e->{
            chamberListView.getSelectionModel().getSelectedItem().addDoor("New Door");
            doorList.clear();
            doorList.addAll(chamberListView.getSelectionModel().getSelectedItem().getDoors());
        });
        stage.setTitle("Java FX demo");
        stage.setScene(scene);
        stage.show();
    }
    public static void main(String [] args) {
        Application.launch(args);
    }

}
